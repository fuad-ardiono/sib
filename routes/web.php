<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/expenditure/pdf/{date_start}/{date_end}', 'Api\PDFController@pdf_expenditure');
Route::get('/inflow/pdf/{date_start}/{date_end}', 'Api\PDFController@pdf_inflow');
Route::get('/history/get/quantity/pdf', 'Api\PDFController@pdf_quantity');
Route::get('/stockcard', 'Api\PDFController@pdf_stockcard');
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
