<?php

use Illuminate\Http\Request;
use App\Consumer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/signup', 'UserController@signup');
Route::post('/login', 'UserController@login');
Route::get('/logout', 'UserController@logout');
Route::get('/validate', 'UserController@validate_cookie');

Route::group(['middleware' => 'jwt-auth'], function () {
    //Consumer module
    Route::post('/consumer/create', 'Api\ConsumerController@create');
    Route::put('/consumer/update', 'Api\ConsumerController@update');
    Route::delete('/consumer/delete/{consumer_no}', 'Api\ConsumerController@delete');
    Route::get('/consumer/{consumer_no}', 'Api\ConsumerController@get');
    Route::get('/consumer/view/all', 'Api\ConsumerController@get_all');
    Route::get('/consumer/view/paginate', 'Api\ConsumerController@paginate_consumer');

    //Config module
    Route::put('/config/update', 'Api\ConfigController@update');
    Route::get('/config/view', 'Api\ConfigController@view');

    //Goods module
    Route::post('/goods/create', 'Api\GoodsController@create');
    Route::put('/goods/update', 'Api\GoodsController@update');
    Route::delete('/goods/delete/{goods_no}', 'Api\GoodsController@delete');
    Route::get('/goods/view/paginate', 'Api\GoodsController@paginate');
    Route::get('/goods/{goods_no}', 'Api\GoodsController@get');
    Route::get('/goods/view/brand/{goods_type}', 'Api\GoodsController@get_brand');
    Route::get('/goods/view/data/{goods_brand}', 'Api\GoodsController@get_goods_data');

    Route::post('/goods/type/create', 'Api\GoodsTypeController@create');
    Route::put('/goods/type/update', 'Api\GoodsTypeController@update');
    Route::delete('/goods/type/delete/{id}', 'Api\GoodsTypeController@delete');
    Route::get('/goods/type/view/paginate', 'Api\GoodsTypeController@paginate');
    Route::get('/goods/type/{id}', 'Api\GoodsTypeController@get');
    Route::get('/goods/type/view/all', 'Api\GoodsTypeController@get_all');

    //Inflow module
    Route::post('/inflow/create', 'Api\InflowController@create');
    Route::put('/inflow/update', 'Api\InflowController@update');
    Route::get('/inflow/{inflow_no}', 'Api\InflowController@get');
    Route::get('/inflow/view/paginate', 'Api\InflowController@paginate');
    Route::delete('/inflow/delete/{inflow_no}', 'Api\InflowController@delete');

    //Expenditure module
    Route::post('/expenditure/create', 'Api\ExpenditureController@create');
    Route::get('/expenditure/view/paginate', 'Api\ExpenditureController@paginate');
    Route::get('/expenditure/{expenditure_no}', 'Api\ExpenditureController@get');
    Route::delete('/expenditure/delete/{expenditure_no}', 'Api\ExpenditureController@delete');
    Route::get('/expenditure/pdf', 'Api\ExpenditureController@pdf');
    Route::get('/expenditure/view/pdf/{date_start}/{date_end}', 'Api\ExpenditureController@viewpdf');

    //History module
    Route::get('/history/get/quantity/{goods_no}', 'Api\HistoryController@get_quantity_specific');
    Route::get('/history/get/quantity', 'Api\HistoryController@get_quantity');
    Route::get('/history/make/data/{history_type}', 'Api\HistoryController@make_history_data');

    //Widget module
    Route::get('/widget/expenditure/today', 'Api\WidgetController@get_expend_today');
    Route::get('/widget/expenditure/month', 'Api\WidgetController@get_expend_month');
    Route::get('/widget/expenditure/year', 'Api\WidgetController@get_expend_year');

    Route::get('/widget/inflow/today', 'Api\WidgetController@get_inflow_today');
    Route::get('/widget/inflow/month', 'Api\WidgetController@get_inflow_month');
    Route::get('/widget/inflow/year', 'Api\WidgetController@get_inflow_year');
});
