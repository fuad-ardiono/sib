<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Barryvdh\DomPDF\PDF;

class History extends Model
{
    protected $table = 'history';

    public static function update_prefix()
    {
        try {
            DB::beginTransaction();

            $config = Config::where('id', 1)->first();
            $config->config_history_count = $config->config_history_count + 1;
            $config->config_history_lastcount = $config->get_last_count($config->config_history_count);
            $config->save();

            $prefix_name = $config->config_history_prefix;
            $prefix_no = $config->config_history_lastcount;
            $prefix = "$prefix_name$prefix_no";

            DB::commit();

            return $prefix;
        } catch (\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function create_inflow_history($data)
    {
        try {
            DB::beginTransaction();

            $prefix = History::update_prefix();

            $history = new History;
            $history->history_no = $prefix;
            $history->history_special_no = $data->inflow_no;
            $history->history_type = 'INSERT INFLOW';
            $history->history_goods_no = $data->inflow_goods_no;
            $history->history_goods_name = $data->inflow_goods_name;
            $history->history_goods_type = $data->inflow_goods_type;
            $history->history_goods_quantity = $data->inflow_goods_quantity;
            $history->save();

            DB::commit();

            return $history;
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function update_inflow_history($data)
    {
        try {
            DB::beginTransaction();

            $quantity_accumulation = 0;
            $inflow_data = History::where('history_special_no', $data->inflow_no)
                            ->whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW'])
                            ->get();

            if(count($inflow_data) !== 0){
                foreach ($inflow_data as $inflow_datum){
                    $quantity_accumulation -= $inflow_datum->history_goods_quantity;
                    $inflow_datum->void = 1;
                    $inflow_datum->save();
                }
            }

            $prefix = History::update_prefix();

            $history = new History;
            $history->history_no = $prefix;
            $history->history_special_no = $data->inflow_no;
            $history->history_type = 'UPDATE INFLOW';
            $history->history_goods_no = $data->inflow_goods_no;
            $history->history_goods_name = $data->inflow_goods_name;
            $history->history_goods_type = $data->inflow_goods_type;
            $history->history_goods_quantity = $quantity_accumulation + $data->inflow_goods_quantity;
            $history->save();

            DB::commit();

            return $history;
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function delete_inflow_history($data)
    {
        try {
            DB::beginTransaction();

            $prefix = History::update_prefix();

            $quantity_accumulation = 0;
            //delete insert inflow history and make accumulation
            $inflow_data = History::where('history_special_no', $data->inflow_no)
                            ->whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW'])
                            ->get();

            if(count($inflow_data) !== 0){
                foreach ($inflow_data as $inflow_datum){
                    //accumulation
                    $quantity_accumulation -= $inflow_datum->history_goods_quantity;
                    //void history
                    $inflow_datum->void = 1;
                    $inflow_datum->save();
                }
            }

            //create delete inflow history
            $history = new History;
            $history->history_no = $prefix;
            $history->history_special_no = $data->inflow_no;
            $history->history_type = 'DELETE INFLOW';
            $history->history_goods_no = $data->inflow_goods_no;
            $history->history_goods_name = $data->inflow_goods_name;
            $history->history_goods_type = $data->inflow_goods_type;
            $history->history_goods_quantity = $quantity_accumulation;
            $history->save();

            DB::commit();

            return $history;
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function create_expenditure_history($data)
    {
        try {
            DB::beginTransaction();

            $prefix = History::update_prefix();

            $history = new History;
            $history->history_no = $prefix;
            $history->history_special_no = $data->expenditure_no;
            $history->history_type = 'INSERT EXPENDITURE';
            $history->history_goods_no = $data->expenditure_goods_no;
            $history->history_goods_name = $data->expenditure_goods_name;
            $history->history_goods_type = $data->expenditure_goods_type;
            $history->history_goods_quantity = $data->expenditure_goods_quantity * -1;
            $history->history_consumer_no = $data->expenditure_consumer_no;
            $history->history_consumer_name = $data->expenditure_consumer_name;
            $history->save();

            DB::commit();

            return $history;
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function delete_expenditure_history($expenditure_no)
    {
        try {
            DB::beginTransaction();

            $prefix = History::update_prefix();

            //delete insert inflow history and make accumulation
            $expenditure_data = History::where('history_special_no', $expenditure_no)
                ->whereIn('history_type', ['INSERT EXPENDITURE', 'UPDATE EXPENDITURE'])
                ->get();

            if(count($expenditure_data) !== 0){
                foreach ($expenditure_data as $expenditure_datum){
                    //accumulation
                    //void history
                    $expenditure_datum->void = 1;
                    $expenditure_datum->save();

                    $history = new History;
                    $history->history_no = $prefix;
                    $history->history_special_no = $expenditure_datum->history_special_no;
                    $history->history_type = 'DELETE EXPENDITURE';
                    $history->history_goods_no = $expenditure_datum->history_goods_no;
                    $history->history_goods_name = $expenditure_datum->history_goods_name;
                    $history->history_goods_type = $expenditure_datum->history_goods_type;
                    $history->history_goods_quantity = $expenditure_datum->history_goods_quantity * -1;
                    $history->history_consumer_no = $expenditure_datum->expenditure_consumer_no;
                    $history->history_consumer_name = $expenditure_datum->expenditure_consumer_name;
                    $history->save();
                }
            }

            DB::commit();

            return $history;
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_quantity_specific($goods_no)
    {
        //getting quantity data real time by specific goods object
        try {
            $quantity = 0;
            $history_data = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW', 'DELETE INFLOW', 'INSERT EXPENDITURE', 'DELETE EXPENDITURE'])
                ->where('history_goods_no', $goods_no)
                ->get();

            foreach ($history_data as $history_datum){
                $quantity += ($history_datum->history_goods_quantity);
            }

            return response()->json($quantity, 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_quantity()
    {
        //getting quantity data real time all goods object
        try {
            $goods = Goods::where('void', 0)->get();
            $goods_quantity = [];

            foreach ($goods as $goods_datum){
                $quantity = 0;
                $history_data = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW', 'INSERT EXPENDITURE'])
                    ->where('history_goods_no', $goods_datum->goods_no)
                    ->where('void', 0)
                    ->get();

                if(count($history_data) == !0) {
                    foreach ( $history_data as $history_datum ) {
                        $quantity += $history_datum->history_goods_quantity;
                    }
                }

                $goods_datum['goods_quantity'] = $quantity;

                array_push($goods_quantity, $goods_datum);
            }

            return response()->json($goods_quantity, 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_quantity_pdf()
    {
        //getting quantity data real time all goods object
        try {
            $goods = Goods::where('void', 0)->get();
            $goods_quantity = [];

            foreach ($goods as $goods_datum){
                $quantity = 0;
                $history_data = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW', 'INSERT EXPENDITURE'])
                    ->where('history_goods_no', $goods_datum->goods_no)
                    ->where('void', 0)
                    ->get();

                if(count($history_data) == !0) {
                    foreach ( $history_data as $history_datum ) {
                        $quantity += $history_datum->history_goods_quantity;
                    }
                }

                $goods_datum['goods_quantity'] = $quantity;

                array_push($goods_quantity, $goods_datum);
            }

            return $goods_quantity;
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function make_history_data($history_type)
    {
        try {
            $history_data = [];

            if($history_type == 'ALL'){
                $history = History::get();

                foreach ($history as $history_datum){
                    $goods = Goods::where('goods_no', $history_datum->history_goods_no)->first();
                    $history_datum['history_goods_brand'] = $goods->goods_brand;

                    if($history_datum['history_type'] === 'INSERT EXPENDITURE'){
                        $history_datum['history_goods_quantity'] = $history_datum['history_goods_quantity'] * -1;
                    }

                    if($history_datum['history_type'] === 'INSERT EXPENDITURE'){
                        $history_datum['history_type'] = 'INSERT PENGELUARAN';
                    } elseif($history_datum['history_type'] === 'INSERT INFLOW'){
                        $history_datum['history_type'] = 'INSERT PEMASUKAN';
                    } elseif ($history_datum['history_type'] === 'DELETE EXPENDITURE'){
                        $history_datum['history_type'] = 'DELETE PENGELUARAN';
                    } elseif($history_datum['history_type'] === 'DELETE INFLOW'){
                        $history_datum['history_type'] = 'DELETE PEMASUKAN';
                    } elseif ($history_datum['history_type'] === 'UPDATE INFLOW'){
                        $history_datum['history_type'] = 'UPDATE PEMASUKAN';
                    }

                    if($history_datum->history_consumer_no == null || $history_datum->history_consumer_name == null){
                        $history_datum['history_consumer_no'] = "-";
                        $history_datum['history_consumer_name'] = "-";
                    }

                    array_push($history_data, $history_datum);
                }
            } elseif($history_type == 'INFLOW') {
                $history = History::where('history_type', 'like', '%INFLOW%')->get();

                foreach ($history as $history_datum){
                    $goods = Goods::where('goods_no', $history_datum->history_goods_no)->first();
                    $history_datum['history_goods_brand'] = $goods->goods_brand;

                    if($history_datum['history_type'] === 'INSERT INFLOW') {
                        $history_datum['history_type'] = "INSERT PEMASUKAN";
                    } elseif ($history_datum['history_type'] === 'UPDATE INFLOW') {
                        $history_datum['history_type'] = "UPDATE PEMASUKAN";
                    } elseif ($history_datum['history_type'] === 'DELETE INFLOW') {
                        $history_datum['history_type'] = "DELETE PEMASUKAN";
                    }

                    if($history_datum->history_consumer_no == null || $history_datum->history_consumer_name == null){
                        $history_datum['history_consumer_no'] = "-";
                        $history_datum['history_consumer_name'] = "-";
                    }

                    array_push($history_data, $history_datum);
                }
            } elseif($history_type == 'EXPENDITURE') {
                $history = History::where('history_type', 'like', '%EXPENDITURE%')->get();

                foreach ($history as $history_datum){
                    $goods = Goods::where('goods_no', $history_datum->history_goods_no)->first();
                    $history_datum['history_goods_brand'] = $goods->goods_brand;

                    if($history_datum['history_type'] === 'INSERT EXPENDITURE') {
                        $history_datum['history_type'] = "INSERT PENGELUARAN";
                    } elseif($history_datum['history_type'] === 'DELETE EXPENDITURE'){
                        $history_datum['history_type'] = 'DELETE PENGELUARAN';
                    }

                    if($history_datum->history_consumer_no == null || $history_datum->history_consumer_name == null){
                        $history_datum['history_consumer_no'] = "-";
                        $history_datum['history_consumer_name'] = "-";
                    }

                    array_push($history_data, $history_datum);
                }
            }

            return response()->json($history_data, 200);

        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_today_expend()
    {
        try {

            $expend = [];
            $value = 0;
            $history = History::where('void', 0)->where('history_type', 'INSERT EXPENDITURE')
                ->where('created_at', '>=',Carbon::today())->get();

            foreach ($history as $history_datum){
                $value -= $history_datum->history_goods_quantity;
            }

            Carbon::setLocale('id');
            $days = Carbon::now()->format('l');
            $data = [(string)$days, $value];

            array_push($expend, $data);

            return response()->json($expend, 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_today_inflow()
    {
        try {

            $expend = [];
            $value = 0;
            $history = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW'])
                ->where('created_at', '>=',Carbon::today())->get();

            foreach ($history as $history_datum){
                $value += $history_datum->history_goods_quantity;
            }

            Carbon::setLocale('id');
            $days = Carbon::now()->format('l');
            $data = [(string)$days, $value];

            array_push($expend, $data);

            return response()->json($expend, 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_month_expend()
    {
        try {
            $expend = [];
            $today = Carbon::now();
            $daysinmonth = Carbon::parse($today)->daysInMonth;
            for ($i=1;$i<=$daysinmonth;$i++){
                $accumulative_date = Carbon::create($today->year, $today->month, $i, 00,00,00, 'Asia/Makassar');
                $history = History::where('void', 0)->where('history_type', 'INSERT EXPENDITURE')
                    ->whereDate('created_at', $accumulative_date)->get();

                if(count($history) == 0){
                    $date_formatted = ''.$today->year.'-'.$today->month.'-'.$i.'';

                    $value = array($date_formatted => 0);

                    array_push($expend, $value);
                } else {
                    $accumulation_qty = 0;
                    foreach ($history as $history_datum){
                        $accumulation_qty -= $history_datum->history_goods_quantity;
                    }
                    $date_formatted = ''.$today->year.'-'.$today->month.'-'.$i.'';

                    $value = array($date_formatted => $accumulation_qty);
                    array_push($expend, $value);
                }
            }

            $data = array_collapse($expend);

            return response()->json($data, 200);

        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_month_inflow()
    {
        try {
            $expend = [];
            $today = Carbon::now();
            $daysinmonth = Carbon::parse($today)->daysInMonth;
            for ($i=1;$i<=$daysinmonth;$i++){
                $accumulative_date = Carbon::create($today->year, $today->month, $i, 00,00,00, 'Asia/Makassar');
                $history = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW'])
                    ->whereDate('created_at', $accumulative_date)->get();

                if(count($history) == 0){
                    $date_formatted = ''.$today->year.'-'.$today->month.'-'.$i.'';

                    $value = array($date_formatted => 0);

                    array_push($expend, $value);
                } else {
                    $accumulation_qty = 0;
                    foreach ($history as $history_datum){
                        $accumulation_qty += $history_datum->history_goods_quantity;
                    }
                    $date_formatted = ''.$today->year.'-'.$today->month.'-'.$i.'';

                    $value = array($date_formatted => $accumulation_qty);
                    array_push($expend, $value);
                }
            }

            $data = array_collapse($expend);

            return response()->json($data, 200);

        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_year_expend()
    {
        try {
            $expend = [];
            $today = Carbon::now();
            $numberOfMonth = 12;

            for($i=1;$i<=$numberOfMonth;$i++){
                $history = History::where('void', 0)->where('history_type', 'INSERT EXPENDITURE')
                    ->whereMonth('created_at', $i)->get();

                if(count($history) == 0){
                    $date_formatted = ''.$today->year.'-'.$i.'';

                    $value = array($date_formatted => 0);

                    array_push($expend, $value);
                } else {
                    $accumulation_qty = 0;
                    foreach ($history as $history_datum){
                        $accumulation_qty -= $history_datum->history_goods_quantity;
                    }
                    $date_formatted = ''.$today->year.'-'.$i.'';

                    $value = array($date_formatted => $accumulation_qty);
                    array_push($expend, $value);
                }
            }

            $data = array_collapse($expend);

            return response()->json($data, 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_year_inflow()
    {
        try {
            $expend = [];
            $today = Carbon::now();
            $numberOfMonth = 12;

            for($i=1;$i<=$numberOfMonth;$i++){
                $history = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW'])
                    ->whereMonth('created_at', $i)->get();

                if(count($history) == 0){
                    $date_formatted = ''.$today->year.'-'.$i.'';

                    $value = array($date_formatted => 0);

                    array_push($expend, $value);
                } else {
                    $accumulation_qty = 0;
                    foreach ($history as $history_datum){
                        $accumulation_qty += $history_datum->history_goods_quantity;
                    }
                    $date_formatted = ''.$today->year.'-'.$i.'';

                    $value = array($date_formatted => $accumulation_qty);
                    array_push($expend, $value);
                }
            }

            $data = array_collapse($expend);

            return response()->json($data, 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function expenditure_pdf($date_start, $date_end)
    {
        $history = History::where('history_type', 'INSERT EXPENDITURE')->where('void', 0);

        if($date_start !== null) {
            $history->where('created_at', '>=', $date_start);
        }

        if($date_start !== null) {
            $history->where('created_at', '<=', $date_end);
        }

        $history_data = $history->get();

        $pdf = [];
        foreach ($history_data as $hd){
            if($hd->history_goods_no !== null){
                $val = Goods::where('goods_no', $hd->history_goods_no)->first();

                $hd['history_goods_brandname'] = $val->goods_brand;
            }
            $hd->history_type = "Melakukan Pengeluaran Barang";
            $hd->history_goods_quantity = $hd->history_goods_quantity * -1;
            array_push($pdf, $hd);
        }

        return $pdf;
    }

    public static function inflow_pdf($date_start, $date_end)
    {
        $inflow = Inflow::where('void', 0);

        if($date_start !== null) {
            $inflow->where('created_at', '>=', $date_start);
        }

        if($date_start !== null) {
            $inflow->where('created_at', '<=', $date_end);
        }

        $inflow_data = $inflow->get();

        $pdf = [];
        foreach ($inflow_data as $id){
            $id['type'] = "Melakukan Pemasukan Barang";

            array_push($pdf, $id);
        }

        return $pdf;
    }



    public static function stock_card_pdf()
    {
        $data_goods = Goods::where('void', 0)->get();
        $data_history = History::whereIn('history_type', ['INSERT INFLOW', 'INSERT EXPENDITURE'])->where('void', 0)->get();
        $stock_card = [];
        $last_stockcard = [];

        foreach ($data_history as $dh){
            foreach ($data_goods as $dg){
                if($dh->history_goods_no == $dg->goods_no){
                    $d['date'] = Carbon::parse($dh->created_at)->format('d-m-Y');
                    $d['hist_no'] = $dh->history_no;
                    $d['goods_no'] = $dg->goods_no;
                    $d['goods_name'] = $dg->goods_name;
                    $d['goods_brand'] = $dg->goods_brand;

                    if($dh->history_type == 'INSERT INFLOW') {
                        $inflow_data = Inflow::where('inflow_no', $dh->history_special_no)->first();
                        $d['type'] = 'INFLOW';
                        $d['goods_stockin'] = $inflow_data->inflow_goods_quantity;
                        $d['goods_stockout'] = 0;

                        $acc = 0;
                        $accumulation = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW', 'INSERT EXPENDITURE'])
                            ->where('history_goods_no', $dh->history_goods_no)
                            ->where('created_at', '<', $dh->created_at)->where('void', 0)->get();

                        foreach($accumulation as $a){
                            $acc += $a->history_goods_quantity;
                        }

                        $d['goods_stockacc'] = $acc + $d['goods_stockin'];

                        array_push($stock_card, $d);

                    } elseif($dh->history_type == 'INSERT EXPENDITURE') {
                        $expenditure_data = Expenditure::where('expenditure_no', $dh->history_special_no)->first();
                        $d['type'] = 'EXPENDITURE';
                        $d['goods_stockin'] = 0;
                        $d['goods_stockout'] = $expenditure_data->expenditure_goods_quantity;

                        $accumulation = History::whereIn('history_type', ['INSERT INFLOW', 'UPDATE INFLOW', 'INSERT EXPENDITURE'])
                            ->where('history_goods_no', $dh->history_goods_no)
                            ->where('created_at', '<', $dh->created_at)->where('void', 0)->get();

                        $acc = 0;
                        foreach($accumulation as $a){
                            $acc += $a->history_goods_quantity;
                        }

                        $d['goods_stockacc'] = $acc - $d['goods_stockout'];

                        array_push($stock_card, $d);
                    }
                }
            }
        }

        $data = collect($stock_card)->groupBy('goods_brand');

        return $data;
    }
}
