<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Config extends Model
{
    protected $table = 'configs';

    public function get_last_count( $count )
    {
        if ( $count < 10 ) {
            return "00" . $count;
        } elseif ( $count < 100 ) {
            return "0" . $count;
        } else {
            return $count;
        }
    }

    public static function update_config($request)
    {
        try{
            DB::beginTransaction();
            $config = Config::find(1);
            $config->config_goods_prefix = $request->config_goods_prefix;
            $config->config_consumer_prefix = $request->config_consumer_prefix;
            $config->config_inflow_prefix = $request->config_inflow_prefix;
            $config->config_expenditure_prefix = $request->config_expenditure_prefix;
            $config->config_asset_prefix = $request->config_asset_prefix;
            $config->config_history_prefix = $request->config_history_prefix;
            $config->save();


            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success to update config'], 200);
        } catch(\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['success' => false, 'message' => 'Failed to update config'], 500);
        }
    }

    public static function view_config()
    {
        try{
            $config = Config::find(1)->first();

            return response()->json(['success' => true, 'data' => $config], 200);
        } catch(\Exception $e){
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['success' => false, 'data' => null], 500);
        }
    }
}
