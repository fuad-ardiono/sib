<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Consumer;
use App\Helper\Collection;

class Expenditure extends Model
{
    protected $table = 'expenditures';

    public static function update_prefix()
    {
        try {
            DB::beginTransaction();

            $config = Config::where('id', 1)->first();
            $config->config_expenditure_count = $config->config_expenditure_count + 1;
            $config->config_expenditure_lastcount = $config->get_last_count($config->config_expenditure_count);
            $config->save();

            $prefix_name = $config->config_expenditure_prefix;
            $prefix_no = $config->config_expenditure_lastcount;
            $prefix = "$prefix_name$prefix_no";

            DB::commit();

            return $prefix;
        } catch (\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function create_expenditure($request)
    {
        try {
            DB::beginTransaction();

            $prefix = Expenditure::update_prefix();

            foreach ($request->expenditure as $expend){
                $consumer_no = Consumer::where('consumer_name', $expend['expenditure_consumer_name'])->first();

                $expenditure = new Expenditure;
                $expenditure->expenditure_no = $prefix;
                $expenditure->expenditure_consumer_no = $consumer_no->consumer_no;
                $expenditure->expenditure_consumer_name = $expend['expenditure_consumer_name'];
                $expenditure->expenditure_goods_no = $expend['expenditure_goods_no'];
                $expenditure->expenditure_goods_name = $expend['expenditure_goods_name'];
                $expenditure->expenditure_goods_type = $expend['expenditure_goods_type'];
                $expenditure->expenditure_goods_brand = $expend['expenditure_goods_brand'];
                $expenditure->expenditure_goods_quantity = $expend['expenditure_goods_quantity'];
                $expenditure->expenditure_goods_allquantity = $expend['expenditure_goods_allquantity'];
                $expenditure->expenditure_date = Carbon::now()->toDateString();

                $history = History::create_expenditure_history($expenditure);

                $expenditure->expenditure_history_no = $history->history_no;
                $expenditure->save();
            }

            DB::commit();
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function paginate_expenditure()
    {
        try {
            $expenditure = Expenditure::where('void', 0)->get();
            $expenditure_filtered = $expenditure->unique('expenditure_no');

            $paginate = (new Collection($expenditure_filtered))->paginate(5);

            return response()->json($paginate, 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_expenditure($expenditure_no)
    {
        try {
            $expenditure = Expenditure::where('void', 0)->where('expenditure_no', $expenditure_no)->get();

            return response()->json(['data' => $expenditure], 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function delete_expenditure($expenditure_no)
    {
        try {
            DB::beginTransaction();
            $expenditure = Expenditure::where('expenditure_no', $expenditure_no)->where('void', 0)->get();

            foreach ($expenditure as $exp){
                $exp->void = 1;
                $exp->save();
            }

            $history = History::delete_expenditure_history($expenditure_no);

            DB::commit();

            return response()->json($history, 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }
}
