<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class Consumer extends Model
{
    protected $table = 'consumers';

    public static function update_prefix()
    {
        try {
            DB::beginTransaction();

            $config = Config::where('id', 1)->first();
            $config->config_consumer_count = $config->config_consumer_count + 1;
            $config->config_consumer_lastcount = $config->get_last_count($config->config_consumer_count);
            $config->save();

            $prefix_name = $config->config_consumer_prefix;
            $prefix_no = $config->config_consumer_lastcount;
            $prefix = "$prefix_name$prefix_no";

            DB::commit();

            return $prefix;
        } catch (\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function create_consumer( $request )
    {
        try{
            DB::beginTransaction();

            $prefix = Consumer::update_prefix();

            $consumer = new Consumer;
            $consumer->consumer_no = $prefix;
            $consumer->consumer_name = $request->consumer_name;
            $consumer->consumer_email = $request->consumer_email;
            $consumer->consumer_phone = $request->consumer_phone;
            $consumer->consumer_website = $request->consumer_website;
            $consumer->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Success create consumer'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json([ 'false' => false, 'message' => 'Failed to create consumer' ], 500);
        }
    }

    public static function update_consumer( $request )
    {
        try{
            DB::beginTransaction();

            $consumer_id = Consumer::where('consumer_no', $request->consumer_no)->where('void', 0)->first();

            $consumer = Consumer::find( $consumer_id->id );
            $consumer->consumer_name = $request->consumer_name;
            $consumer->consumer_email = $request->consumer_email;
            $consumer->consumer_phone = $request->consumer_phone;
            $consumer->consumer_website = $request->consumer_website;
            $consumer->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Success update consumer'], 200);
        } catch(\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['false' => false, 'message' => 'Failed to update consumer'], 500);
        }
    }

    public static function delete_consumer( $consumer_no )
    {
        try{
            DB::beginTransaction();

            $consumer_id = Consumer::where('consumer_no', $consumer_no)->where('void', 0)->first();
            $consumer = Consumer::find($consumer_id->id);
            $consumer->void = 1;
            $consumer->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Success delete consumer'], 200);
        } catch(\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['success' => false, 'message' => 'Failed to delete consumer'], 500);
        }
    }

    public static function get_consumer( $consumer_no )
    {
        try{
            $consumer = Consumer::where('consumer_no', $consumer_no)->where('void', 0)->get();

            return response()->json(['sucess' => true, 'data' => $consumer], 200);
        } catch(\Exception $e){
          var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['sucess' => false, 'data' => null], 500);
        }
    }

    public static function get_all_consumer()
    {
        try{
            $consumer = Consumer::where('void', 0)->pluck('consumer_name')->toArray();

            return response()->json($consumer, 200);
        } catch(\Exception $e){
          var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['sucess' => false, 'data' => null], 500);
        }
    }

    public static function paginate_consumer()
    {
        try{
            $consumer = Consumer::where('void', 0)->paginate(5);

            return response()->json($consumer, 200);
        } catch(\Exception $e){
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['sucess' => false, 'data' => null], 500);
        }
    }
}
