<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model
{
    protected $table = 'goods';

    public static function update_prefix()
    {
        try {
            DB::beginTransaction();

            $config = Config::where('id', 1)->first();
            $config->config_goods_count = $config->config_goods_count + 1;
            $config->config_goods_lastcount = $config->get_last_count($config->config_goods_count);
            $config->save();

            $prefix_name = $config->config_goods_prefix;
            $prefix_no = $config->config_goods_lastcount;
            $prefix = "$prefix_name$prefix_no";

            DB::commit();

            return $prefix;
        } catch (\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function create_goods($request)
    {
        try {
            DB::beginTransaction();

            $prefix = Goods::update_prefix();

            $goods = new Goods;
            $goods->goods_no = $prefix;
            $goods->goods_name = $request->goods_name;
            $goods->goods_brand = $request->goods_brand;
            $goods->goods_barcode = $request->goods_barcode;
            $goods->goods_type = $request->goods_type;
            $goods->save();

            DB::commit();

            return response()->json(['success' => true, 'data' => $goods], 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['success' => true, 'data' => $goods], 500);
        }
    }

    public static function update_goods($request)
    {
        try {
            DB::beginTransaction();

            $goods_id = Goods::where('goods_no', $request->goods_no)->where('void', 0)->first();

            $goods = Goods::find($goods_id->id);
            $goods->goods_name = $request->goods_name;
            $goods->goods_brand = $request->goods_brand;
            $goods->goods_barcode = $request->goods_barcode;
            $goods->goods_type = $request->goods_type;
            $goods->save();

            DB::commit();

            return response()->json(['success' => true, 'data' => $goods], 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function delete_goods( $goods_no )
    {
        try {
            DB::beginTransaction();

            $goods_id = Goods::where('goods_no', $goods_no)->where('void', 0)->first();
            $goods = Goods::find($goods_id->id);
            $goods->void = 1;
            $goods->save();

            DB::commit();

            return response()->json(['success' => true], 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_goods( $goods_no )
    {
        try {
            $goods = Goods::where('goods_no', $goods_no)->where('void', 0)->get();

            return response()->json(['success' => true, 'data' => $goods], 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_goods_brand($goods_type)
    {
        try {
            $goods = Goods::where('goods_type', $goods_type)->where('void', 0)->pluck('goods_brand')->toArray();

            return response()->json($goods, 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_goods_data($goods_brand)
    {
        try {
            $goods = Goods::where('void', 0)->where('goods_brand', $goods_brand)->first();

            return response()->json($goods, 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function paginate_goods()
    {
        try{
            $goods = Goods::where('void', 0)->paginate(5);

            return response()->json($goods, 200);
        } catch(\Exception $e){
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['sucess' => false, 'data' => null], 500);
        }
    }
}
