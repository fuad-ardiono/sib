<?php

namespace App\Http\Controllers\Api;

use App\Expenditure;
use App\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;

class ExpenditureController extends Controller
{
    public function create(Request $request)
    {
        $expenditure = Expenditure::create_expenditure($request);

        return $expenditure;
    }

    public function paginate()
    {
        $expenditure = Expenditure::paginate_expenditure();

        return $expenditure;
    }

    public function get($expenditure_no)
    {
        $expenditure = Expenditure::get_expenditure($expenditure_no);

        return $expenditure;
    }

    public function delete($expenditure_no)
    {
        $expenditure = Expenditure::delete_expenditure($expenditure_no);

        return $expenditure;
    }
}
