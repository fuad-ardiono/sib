<?php

namespace App\Http\Controllers\Api;

use App\History;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use PDF;

class HistoryController extends Controller
{
    public function get_quantity_specific($goods_no)
    {
        $history = History::get_quantity_specific($goods_no);

        return $history;
    }

    public function make_history_data($history_type)
    {
        $history = History::make_history_data($history_type);

        return $history;
    }

    public function get_quantity()
    {
        $history = History::get_quantity();

        return $history;
    }
}
