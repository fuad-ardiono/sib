<?php

namespace App\Http\Controllers\Api;

use App\History;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WidgetController extends Controller
{
    public function get_expend_today()
    {
        $history = History::get_today_expend();

        return $history;
    }

    public function get_expend_month()
    {
        $history = History::get_month_expend();

        return $history;
    }

    public function get_expend_year()
    {
        $history = History::get_year_expend();

        return $history;
    }
    public function get_inflow_today()
    {
        $history = History::get_today_inflow();

        return $history;
    }

    public function get_inflow_month()
    {
        $history = History::get_month_inflow();

        return $history;
    }

    public function get_inflow_year()
    {
        $history = History::get_year_inflow();

        return $history;
    }
}
