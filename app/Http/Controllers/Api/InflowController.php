<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Inflow;
use Illuminate\Support\Facades\Cookie;
use PDF;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\History;

class InflowController extends Controller
{
    public function create(Request $request)
    {
        $inflow = Inflow::create_inflow($request);

        return $inflow;
    }

    public function delete($inflow_no)
    {
        $inflow = Inflow::delete_inflow($inflow_no);

        return $inflow;
    }

    public function update(Request $request)
    {
        $inflow = Inflow::update_inflow($request);

        return $inflow;
    }

    public function get($inflow_no)
    {
        $inflow = Inflow::get_inflow($inflow_no);

        return $inflow;
    }

    public function paginate()
    {
        $inflow = Inflow::paginate_inflow();

        return $inflow;
    }
}
