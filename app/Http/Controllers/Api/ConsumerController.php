<?php

namespace App\Http\Controllers\Api;

use App\Consumer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConsumerController extends Controller
{
    public function create(Request $request)
    {
        $consumer = Consumer::create_consumer($request);

        return $consumer;
    }

    public function update(Request $request)
    {
        $consumer = Consumer::update_consumer($request);

        return $consumer;
    }

    public function delete($consumer_no)
    {
        $consumer = Consumer::delete_consumer($consumer_no);

        return $consumer;
    }

    public function get($consumer_no)
    {
        $consumer = Consumer::get_consumer($consumer_no);

        return $consumer;
    }

    public function get_all()
    {
        $consumer = Consumer::get_all_consumer();

        return $consumer;
    }

    public function paginate_consumer()
    {
        $consumer = Consumer::paginate_consumer();

        return $consumer;
    }
}
