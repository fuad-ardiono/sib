<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use App\History;
use PDF;
use App\Goods;
use Illuminate\Support\Facades\Redirect;

class PDFController extends Controller
{
    public function pdf_quantity()
    {
        $cookie = Cookie::get('token');

        if($cookie){
            $data = History::get_quantity_pdf();

            $pdf = PDF::loadView('pdf/stock', array('stock' => $data));
            $pdf->setPaper('A4', 'landscape');
            return $pdf->stream();
        } else {
            return Redirect::to('/');
        }
    }

    public function pdf_expenditure(Request $request)
    {
        $cookie = Cookie::get('token');

        if($cookie){

            if($request->date_start !== 0) {
                $date_start = Carbon::parse($request->date_start);
            } else {
                $date_start = null;
            }

            if($request->date_end !== 0) {
                $date_end = Carbon::parse($request->date_end);
            } else {
                $date_end = null;
            }
            $data = History::expenditure_pdf($date_start,$date_end);

            $pdf = PDF::loadView('pdf/expenditure', array('expenditure' => $data, 'date_start' => $date_start, 'date_end' => $date_end));
            $pdf->setPaper('A4', 'landscape');
            return $pdf->stream();
        } else {
            return Redirect::to('/');
        }
    }

    public function pdf_inflow(Request $request)
    {
        $cookie = Cookie::get('token');

        if($cookie){

            if($request->date_start !== 0) {
                $date_start = Carbon::parse($request->date_start);
            } else {
                $date_start = null;
            }

            if($request->date_end !== 0) {
                $date_end = Carbon::parse($request->date_end);
            } else {
                $date_end = null;
            }
            $data = History::inflow_pdf($date_start,$date_end);

            $pdf = PDF::loadView('pdf/inflow', array('inflow' => $data, 'date_start' => $date_start, 'date_end' => $date_end));
            $pdf->setPaper('A4', 'landscape');
            return $pdf->stream();
        } else {
            return Redirect::to('/');
        }
    }

    public static function pdf_stockcard()
    {
        $data = History::stock_card_pdf();

        $goods = Goods::where('void', 0)->get();

        $goods_brand = [];

        foreach ($goods as $g){
            $history = History::where('history_goods_no', $g->goods_no)
                ->where('void', 0)
                ->whereNotIn('history_type',['DELETE INFLOW'])
                ->first();

            if($history){
                array_push($goods_brand, $g->goods_brand);
            }
        }

        $pdf = PDF::loadView('pdf/stockcard', array('stock_card' => $data, 'goods_brand' => $goods_brand));
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream();
    }
}
