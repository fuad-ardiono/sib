<?php

namespace App\Http\Controllers\Api;

use App\Goods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GoodsController extends Controller
{
    public function create(Request $request){
        $goods = Goods::create_goods($request);

        return $goods;
    }

    public function update(Request $request){
        $goods = Goods::update_goods($request);

        return $goods;
    }

    public function delete($goods_no){
        $goods = Goods::delete_goods($goods_no);

        return $goods;
    }

    public function get($goods_no){
        $goods = Goods::get_goods($goods_no);

        return $goods;
    }

    public function get_brand($goods_type){
        $goods = Goods::get_goods_brand($goods_type);

        return $goods;
    }

    public function get_goods_data($goods_brand){
        $goods = Goods::get_goods_data($goods_brand);

        return $goods;
    }

    public function paginate(){
        $goods = Goods::paginate_goods();

        return $goods;
    }
}
