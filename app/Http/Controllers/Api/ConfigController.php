<?php

namespace App\Http\Controllers\Api;

use App\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    public function update(Request $request)
    {
        $config = Config::update_config($request);

        return $config;
    }

    public function view()
    {
        $config = Config::view_config();

        return $config;
    }
}