<?php

namespace App\Http\Controllers\Api;

use App\GoodsType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoodsTypeController extends Controller
{
    public function create(Request $request)
    {
        $goods_type = GoodsType::create_goods_type($request);

        return $goods_type;
    }

    public function update(Request $request)
    {
        $goods_type = GoodsType::update_goods_type($request);

        return $goods_type;
    }

    public function delete($id)
    {
        $goods_type = GoodsType::delete_goods_type($id);

        return $goods_type;
    }

    public function paginate()
    {
        $goods_type = GoodsType::paginate_goods_type();

        return $goods_type;
    }

    public function get($id)
    {
        $goods_type = GoodsType::get_goods_type($id);

        return $goods_type;
    }

    public function get_all()
    {
        $goods_type = GoodsType::get_all_good_type();

        return $goods_type;
    }
}
