<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inflow extends Model
{
    protected $table = 'inflows';

    public static function update_prefix()
    {
        try {
            DB::beginTransaction();

            $config = Config::where('id', 1)->first();
            $config->config_inflow_count = $config->config_inflow_count + 1;
            $config->config_inflow_lastcount = $config->get_last_count($config->config_inflow_count);
            $config->save();

            $prefix_name = $config->config_inflow_prefix;
            $prefix_no = $config->config_inflow_lastcount;
            $prefix = "$prefix_name$prefix_no";

            DB::commit();

            return $prefix;
        } catch (\Exception $e){
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function create_inflow($request)
    {
        try {
            DB::beginTransaction();

            $prefix = Inflow::update_prefix();

            //insert inflow data to db
            $inflow = new Inflow;
            $inflow->inflow_no = $prefix;
            $inflow->inflow_goods_no = $request->inflow_goods_no;
            $inflow->inflow_goods_name = $request->inflow_goods_name;
            $inflow->inflow_goods_brand = $request->inflow_goods_brand;
            $inflow->inflow_goods_type = $request->inflow_goods_type;
            $inflow->inflow_goods_quantity = $request->inflow_goods_quantity;
            $inflow->inflow_date = Carbon::now()->toDateString();

            $history = History::create_inflow_history($inflow);

            $inflow->inflow_history_no = $history->history_no;
            $inflow->save();

            DB::commit();

            return response()->json(['success' => true, 'data' => $inflow],200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function delete_inflow($inflow_no)
    {
        try {
            DB::beginTransaction();

            $inflow_id = Inflow::where('inflow_no', $inflow_no)->where('void', 0)->first();
            $inflow = Inflow::find($inflow_id->id);
            $inflow->void = 1;
            $inflow->save();

            $history = History::delete_inflow_history($inflow);

            DB::commit();
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function update_inflow($request)
    {
        try {
            DB::beginTransaction();

            $inflow_id = Inflow::where('inflow_no', $request->inflow_no)->where('void', 0)->first();
            $inflow = Inflow::find($inflow_id->id);
            $inflow->inflow_goods_quantity = $request->inflow_goods_quantity;

            $history = History::update_inflow_history($inflow);

            $inflow->inflow_history_no = $history->history_no;
            $inflow->save();

            DB::commit();

            return response()->json(['success' => true, 'data' => $inflow],200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function get_inflow($inflow_no)
    {
        try {
            $inflow = Inflow::where('void', 0)->where('inflow_no', $inflow_no)->get();

            return response()->json(['data' => $inflow], 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }

    public static function paginate_inflow()
    {
        try {
            $inflow = Inflow::where('void', 0)->paginate(5);

            return response()->json($inflow, 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }
    }
}
