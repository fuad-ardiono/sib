<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class GoodsType extends Model
{
    protected $table = 'goods_type';

    public static function create_goods_type($request)
    {
        try {
            DB::beginTransaction();

            $goods_type = new GoodsType;
            $goods_type->goods_type_name = $request->goods_type_name;
            $goods_type->save();

            DB::commit();
            return response()->json(['success' => true, 'data' => $goods_type], 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['success' => false, 'data' => null], 500);
        }
    }

    public static function update_goods_type($request)
    {
        try {
            DB::beginTransaction();

            $goods_type = GoodsType::find($request->id);
            $goods_type->goods_type_name = $request->goods_type_name;
            $goods_type->save();

            DB::commit();
            return response()->json(['success' => true, 'data' => $goods_type], 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['success' => false, 'data' => null], 500);
        }
    }

    public static function delete_goods_type($id)
    {
        try {
            DB::beginTransaction();

            $goods_type = GoodsType::find($id);
            $goods_type->void = 1;
            $goods_type->save();

            DB::commit();
            return response()->json(['success' => true, 'data' => $goods_type], 200);
        } catch ( \Exception $e ) {
            DB::rollBack();
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['success' => false, 'data' => null], 500);
        }
    }

    public static function get_goods_type($id)
    {
        try {
            $goods_type = GoodsType::where('id', $id)->where('void', 0)->get();

            return response()->json(['success' => true, 'data' => $goods_type], 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['sucess' => false, 'data' => null], 500);
        }
    }

    public static function get_all_good_type()
    {
        try {
            $goods_type = GoodsType::where('void', 0)->pluck('goods_type_name')->toArray();

            return response()->json($goods_type, 200);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['sucess' => false, 'data' => null], 500);
        }
    }

    public static function paginate_goods_type()
    {
        try{
            $goods_type = GoodsType::where('void', 0)->paginate(5);

            return response()->json($goods_type, 200);
        } catch(\Exception $e){
            var_dump($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
            return response()->json(['sucess' => false, 'data' => null], 500);
        }
    }
}
