<?php
/**
 * Created by PhpStorm.
 * User: fuadardiono
 * Date: 25/06/18
 * Time: 15.40
 */

namespace App\Helper;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection as BaseCollection;

class Collection
{
    public $param;

    public function __construct($param)
    {
        $this->param = $param;
    }

    public function paginate($perPage, $total = null, $page = null, $pageName = 'page')
    {
        $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
        return new LengthAwarePaginator($this->param->forPage($page, $perPage), $total ?: $this->param->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }
}