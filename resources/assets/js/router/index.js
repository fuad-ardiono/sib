import Vue from 'vue';
import Router from 'vue-router';

// Containers
import Full from '../containers/Full';

// Views
import Dashboard from '../views/Dashboard';
import Charts from '../views/Charts';
import Widgets from '../views/Widgets';

// Views - Components
import Buttons from '../views/components/Buttons';
import SocialButtons from '../views/components/SocialButtons';
import Cards from '../views/components/Cards';
import Forms from '../views/components/Forms';
import Modals from '../views/components/Modals';
import Switches from '../views/components/Switches';
import Tables from '../views/components/Tables';

import Configs from '../views/components/Configs';
import Consumers from '../views/components/Consumers';
import Goods from '../views/components/Goods';
import GoodsType from '../views/components/GoodsType';
import Inflows from '../views/components/Inflows';
import Expenditure from '../views/components/Expenditure';
import History from '../views/components/History';
import Document from '../views/components/Document';
import Stock from '../views/components/Stock';

// Views - Icons
import FontAwesome from '../views/icons/FontAwesome';
import SimpleLineIcons from '../views/icons/SimpleLineIcons';

// Views - Pages
import Page404 from '../views/pages/Page404';
import Page500 from '../views/pages/Page500';
import Login from '../views/pages/Login';
import Register from '../views/pages/Register';

Vue.use(Router);

export default new Router({
    mode: 'hash', // Demo is living in GitHub.io, so required!
    linkActiveClass: 'open active',
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '/home',
            redirect: '/home/dashboard',
            name: 'Home',
            component: Full,
            children: [
                {
                    path: '/home/dashboard',
                    name: 'Dashboard',
                    component: Dashboard,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/configs',
                    name: 'Konfigurasi',
                    component: Configs,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/consumers',
                    name: 'Konsumer',
                    component: Consumers,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/goods',
                    redirect: '/home/goods/mgoods',
                    name: 'Barang',
                    component: {
                        render(c) {
                            return c('router-view');
                        }
                    },
                    children:[
                        {
                            path: 'mgoods',
                            name: 'Master Barang',
                            component: Goods,
                            meta: {
                                progress: {
                                    func: [
                                        {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                        {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                        {call: 'location', modifier: 'temp', argument: 'top'},
                                        {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                                    ]
                                }
                            }
                        },
                        {
                            path: 'goodstype',
                            name: 'Tipe Barang',
                            component: GoodsType,
                            meta: {
                                progress: {
                                    func: [
                                        {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                        {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                        {call: 'location', modifier: 'temp', argument: 'top'},
                                        {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                                    ]
                                }
                            }
                        }
                    ]
                },
                {
                    path: '/home/inflow',
                    name: 'Pemasukan',
                    component: Inflows,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/stock',
                    name: 'Stock Barang',
                    component: Stock,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/expenditure',
                    name: 'Pengeluaran',
                    component: Expenditure,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/history',
                    name: 'Riwayat',
                    component: History,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/document',
                    name: 'Buat Dokumen',
                    component: Document,
                    meta: {
                        progress: {
                            func: [
                                {call: 'color', modifier: 'temp', argument: '#2a363d'},
                                {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                                {call: 'location', modifier: 'temp', argument: 'top'},
                                {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                            ]
                        }
                    }
                },
                {
                    path: '/home/charts',
                    name: 'Charts',
                    component: Charts
                },
                {
                    path: '/home/widgets',
                    name: 'Widgets',
                    component: Widgets
                },
                {
                    path: '/home/components',
                    redirect: '/home/components/buttons',
                    name: 'Components',
                    component: {
                        render(c) {
                            return c('router-view');
                        }
                    },
                    children: [
                        {
                            path: 'buttons',
                            name: 'Buttons',
                            component: Buttons
                        },
                        {
                            path: 'social-buttons',
                            name: 'Social Buttons',
                            component: SocialButtons
                        },
                        {
                            path: 'cards',
                            name: 'Cards',
                            component: Cards
                        },
                        {
                            path: 'forms',
                            name: 'Forms',
                            component: Forms
                        },
                        {
                            path: 'modals',
                            name: 'Modals',
                            component: Modals
                        },
                        {
                            path: 'switches',
                            name: 'Switches',
                            component: Switches
                        },
                        {
                            path: 'tables',
                            name: 'Tables',
                            component: Tables
                        }
                    ]
                },
                {
                    path: '/home/icons',
                    redirect: '/home/icons/font-awesome',
                    name: 'Icons',
                    component: {
                        render(c) {
                            return c('router-view');
                        }
                    },
                    children: [
                        {
                            path: 'font-awesome',
                            name: 'Font Awesome',
                            component: FontAwesome
                        },
                        {
                            path: 'simple-line-icons',
                            name: 'Simple Line Icons',
                            component: SimpleLineIcons
                        }
                    ]
                }
            ]
        },
        {
            path: '/',
            redirect: '/login',
            name: 'Pages',
            component: {
                render(c) {
                    return c('router-view');
                }
            },
            children: [
                {
                    path: '404',
                    name: 'Page404',
                    component: Page404
                },
                {
                    path: '500',
                    name: 'Page500',
                    component: Page500
                },
                {
                    path: 'login',
                    name: 'Login',
                    component: Login
                },
                {
                    path: 'register',
                    name: 'Register',
                    component: Register
                }
            ]
        },
        {
            path: '*',
            redirect: '/404',
            name: 'Pages',
            component: {
                render(c) {
                    return c('router-view');
                }
            }
        }
    ]
});
