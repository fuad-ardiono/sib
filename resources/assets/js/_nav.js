export default {
    items: [
        {
            name: 'Dashboard',
            url: '/home/dashboard',
            icon: 'icon-grid'
        },
        {
            title: true,
            name: 'Sistem'
        },
        {
            name: 'Konfigurasi',
            url: '/home/configs',
            icon: 'icon-settings'
        },
        {
            title: true,
            name: 'Aktivitas'
        },
        {
            name: 'Konsumer',
            url: '/home/consumers',
            icon: 'icon-user'
        },
        {
            name: 'Barang',
            url: '/home/goods',
            icon: 'icon-layers',
            children: [
                {
                    name: 'Master Barang',
                    url: '/home/goods/mgoods',
                    icon: 'icon-notebook'
                },
                {
                    name: 'Tipe Barang',
                    url: '/home/goods/goodstype',
                    icon: 'icon-paper-clip'
                }
            ]
        },
        {
            name: 'Pemasukan',
            url: '/home/inflow',
            icon: 'icon-drawer'
        },
        {
            name: 'Pengeluaran',
            url: '/home/expenditure',
            icon: 'icon-paper-plane'
        },
        {
            name: 'Riwayat',
            url: '/home/history',
            icon: 'icon-list'
        },
        {
            name: 'Cek Stock Barang',
            url: '/home/stock',
            icon: 'icon-speedometer'
        },
        {
            name: 'Buat Dokumen',
            url: '/home/document',
            icon: 'icon-docs'
        },
        // {
        //     title: true,
        //     name: 'UI elements'
        // },
        // {
        //     name: 'Components',
        //     url: '/home/components',
        //     icon: 'icon-puzzle',
        //     children: [
        //         {
        //             name: 'Buttons',
        //             url: '/home/components/buttons',
        //             icon: 'icon-puzzle'
        //         },
        //         {
        //             name: 'Social Buttons',
        //             url: '/home/components/social-buttons',
        //             icon: 'icon-puzzle'
        //         },
        //         {
        //             name: 'Cards',
        //             url: '/home/components/cards',
        //             icon: 'icon-puzzle'
        //         },
        //         {
        //             name: 'Forms',
        //             url: '/home/components/forms',
        //             icon: 'icon-puzzle'
        //         },
        //         {
        //             name: 'Modals',
        //             url: '/home/components/modals',
        //             icon: 'icon-puzzle'
        //         },
        //         {
        //             name: 'Switches',
        //             url: '/home/components/switches',
        //             icon: 'icon-puzzle'
        //         },
        //         {
        //             name: 'Tables',
        //             url: '/home/components/tables',
        //             icon: 'icon-puzzle'
        //         }
        //     ]
        // },
        // {
        //     name: 'Icons',
        //     url: '/home/icons',
        //     icon: 'icon-star',
        //     children: [
        //         {
        //             name: 'Font Awesome',
        //             url: '/home/icons/font-awesome',
        //             icon: 'icon-star'
        //         },
        //         {
        //             name: 'Simple Line Icons',
        //             url: '/home/icons/simple-line-icons',
        //             icon: 'icon-star'
        //         }
        //     ]
        // },
        // {
        //     name: 'Widgets',
        //     url: '/home/widgets',
        //     icon: 'icon-calculator',
        //     badge: {
        //         variant: 'danger',
        //         text: 'NEW'
        //     }
        // },
        // {
        //     name: 'Charts',
        //     url: '/home/charts',
        //     icon: 'icon-pie-chart'
        // },
        // {
        //     divider: true
        // },
        // {
        //     title: true,
        //     name: 'Extras'
        // },
        // {
        //     name: 'Pages',
        //     url: '/pages',
        //     icon: 'icon-star',
        //     children: [
        //         {
        //             name: 'Login',
        //             url: '/login',
        //             icon: 'icon-star'
        //         },
        //         {
        //             name: 'Register',
        //             url: '/register',
        //             icon: 'icon-star'
        //         },
        //         {
        //             name: 'Error 404',
        //             url: '/404',
        //             icon: 'icon-star'
        //         },
        //         {
        //             name: 'Error 500',
        //             url: '/500',
        //             icon: 'icon-star'
        //         }
        //     ]
        // }
    ]
};
