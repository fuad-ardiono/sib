// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import Vuetable from 'vuetable-2';
import VueProgressBar from 'vue-progressbar'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'

const options = {
    color: '#2a363d',
    failedColor: '#b71c1c',
    thickness: '5px',
    transition: {
        speed: '0.2s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
}

Vue.use(BootstrapVue);
Vue.use(Vuetable);
Vue.use(VueProgressBar, options);
Vue.use(VueChartkick, {adapter: Chart})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
