<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>stockbarang-{{Carbon\Carbon::now()->format('Y-m-d')}}</title>
</head>
<body>
<style>
    #data {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #data td, #data th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #data tr:nth-child(even){background-color: #f2f2f2;}

    #data tr:hover {background-color: #ddd;}

    #data th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: white;
        color: black;
    }
</style>
<img src="images/kopsurat.png" alt="" style="width:100%;height: 20%;">
<h3>Stock Barang</h3>

<table id="data">
    <tr>
        <th>Tanggal</th>
        <th>Tipe Barang</th>
        <th>Nama Barang</th>
        <th>Nama Brand</th>
        <th>Kuantitas Barang</th>

    </tr>

    @foreach($stock as $s)
        <tr>
            <td>{{ Carbon\Carbon::parse($s->created_at)->format('d-m-Y')  }}</td>
            <td>{{ $s->goods_type  }}</td>
            <td>{{ $s->goods_name }}</td>
            <td>{{ $s->goods_brand }}</td>
            <td>{{ $s->goods_quantity  }}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="4">Total Barang Yang Ada Di Gudang</th>
        <td>
            @php
                $val_arr = 0;
            @endphp
        @foreach($stock as $s)
            @php
                $val_arr += $s->goods_quantity
            @endphp
        @endforeach
            @php
                echo $val_arr;
            @endphp
        </td>
    </tr>


</table>

<div style="float: right">
    <p>Balikpapan, {{ Carbon\Carbon::now('Asia/Makassar')->format('d-m-Y')  }}<br>
        Unit Sarana Politeknik Negeri Balikpapan</p>
</div>
</body>
</html>