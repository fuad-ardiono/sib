<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Stock Card-{{\Carbon\Carbon::now('Asia/Makassar')->format('d-m-Y')}}</title>
    <style>
        #data {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #data td, #data th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #data tr:nth-child(even){background-color: #f2f2f2;}

        #data tr:hover {background-color: #ddd;}

        #data th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: white;
            color: black;
        }

        div.breakNow {
            page-break-after:always;
        }
    </style>
</head>
<body>
@for($i=0;$i<count($goods_brand);$i++)
    <img src="images/kopsurat.png" alt="" style="width:100%;height: 40%;">
<h3>Stock Card</h3>
<p>Nama barang : {{ $goods_brand[$i]  }}</p>
<table id="data">
    <tr>
        <th>Tanggal</th>
        <th>Keterangan</th>
        <th>Barang Masuk</th>
        <th>Barang Keluar</th>
        <th>Sisa</th>

    </tr>

    @foreach($stock_card[$goods_brand[$i]] as $s)
        <tr>
            <td>{{ Carbon\Carbon::parse($s['date'])->format('d-m-Y')  }}</td>
            <td>
                @if($s['type'] == 'EXPENDITURE')
                    PENGELUARAN
                @elseif($s['type'] == 'INFLOW')
                    PEMASUKAN
                @endif
            </td>

            <td>
                @if($s['type'] == 'EXPENDITURE')
                    -
                @elseif($s['type'] == 'INFLOW')
                    {{ $s['goods_stockin']  }}
                @endif
            </td>
            <td>
                @if($s['type'] == 'EXPENDITURE')
                    {{ $s['goods_stockout']  }}
                @elseif($s['type'] == 'INFLOW')
                    -
                @endif
            </td>
            <td>{{ $s['goods_stockacc']  }}</td>
        </tr>
    @endforeach
</table>

    <div style="float: right">
        <p>Balikpapan, {{ Carbon\Carbon::now('Asia/Makassar')->format('d-m-Y')  }}<br>
            Unit Sarana Politeknik Negeri Balikpapan</p>
    </div>
<div class="breakNow"></div>
@endfor
</body>
</html>