<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ Carbon\Carbon::parse($date_start)->format('Y-m-d')  }}sampai{{ Carbon\Carbon::parse($date_end)->format('Y-m-d')  }}-pengeluaran</title>
    <style>
        #data {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #data td, #data th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #data tr:nth-child(even){background-color: #f2f2f2;}

        #data tr:hover {background-color: #ddd;}

        #data th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: white;
            color: black;
        }
    </style>
</head>
<body>
<img src="images/kopsurat.png" alt="" style="width:100%;height: 20%;">
<h3>Pemasukan Barang</h3>
<p>Dari tanggal : {{ Carbon\Carbon::parse($date_start)->format('Y-m-d')  }}<br>
Sampai tanggal : {{ Carbon\Carbon::parse($date_end)->format('Y-m-d')  }}</p>

<table id="data">
    <tr>
        <th>Tanggal</th>
        <th>Tipe Pemasukan</th>
        <th>Nama Barang</th>
        <th>Nama Brand</th>
        <th>Tipe Barang</th>
        <th>Kuantitas Barang</th>

    </tr>

    @foreach($inflow as $i)
        <tr>
            <td>{{ Carbon\Carbon::parse($i->created_at)->format('Y-m-d')  }}</td>
            <td>{{ $i->type  }}</td>
            <td>{{ $i->inflow_goods_name }}</td>
            <td>{{ $i->inflow_goods_brand  }}</td>
            <td>{{ $i->inflow_goods_type  }}</td>
            <td>{{ $i->inflow_goods_quantity  }}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="5">Total Barang Yang Masuk Ke Gudang</th>
        <td>
            @php
                $val_arr = 0;
            @endphp
        @foreach($inflow as $i)
            @php
                $val_arr += $i->inflow_goods_quantity
            @endphp
        @endforeach
            @php
                echo $val_arr;
            @endphp
        </td>
    </tr>
</table>


<div style="float: right">
    <p>Balikpapan, {{ Carbon\Carbon::now('Asia/Makassar')->format('d-m-Y')  }}<br>
        Unit Sarana Politeknik Negeri Balikpapan</p>
</div>
</body>
</html>