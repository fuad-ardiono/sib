<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ Carbon\Carbon::parse($date_start)->format('Y-m-d')  }}sampai{{ Carbon\Carbon::parse($date_end)->format('Y-m-d')  }}-pemasukan</title>
    <style>
        #data {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #data td, #data th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #data tr:nth-child(even){background-color: #f2f2f2;}

        #data tr:hover {background-color: #ddd;}

        #data th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: white;
            color: black;
        }
    </style>
</head>
<body>
<img src="images/kopsurat.png" alt="" style="width:100%;height: 20%;">
<h3>Pengeluaran Barang</h3>
<p>Dari tanggal : {{ Carbon\Carbon::parse($date_start)->format('d-m-Y')  }}<br>
Sampai tanggal : {{ Carbon\Carbon::parse($date_end)->format('d-m-Y')  }}</p>

<table id="data">
    <tr>
        <th>Tanggal</th>
        <th>Tipe Pengeluaran</th>
        <th>Konsumer</th>
        <th>Nama Barang</th>
        <th>Nama Brand</th>
        <th>Tipe Barang</th>
        <th>Kuantitas Barang</th>

    </tr>

    @foreach($expenditure as $e)
        <tr>
            <td>{{ Carbon\Carbon::parse($e->created_at)->format('d-m-Y')  }}</td>
            <td>{{ $e->history_type  }}</td>
            <td>{{ $e->history_consumer_name  }}</td>
            <td>{{ $e->history_goods_name }}</td>
            <td>{{ $e->history_goods_brandname  }}</td>
            <td>{{ $e->history_goods_type  }}</td>
            <td>{{ $e->history_goods_quantity  }}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="6">Total Barang Yang Keluar Dari Gudang</th>
        <td>
            @php
                $val_arr = 0;
            @endphp
        @foreach($expenditure as $e)
            @php
                $val_arr += $e->history_goods_quantity
            @endphp
        @endforeach
            @php
                echo $val_arr;
            @endphp
        </td>
    </tr>


</table>



<div style="float: right">
    <p>Balikpapan, {{ Carbon\Carbon::now('Asia/Makassar')->format('d-m-Y')  }}<br>
        Unit Sarana Politeknik Negeri Balikpapan</p>
</div>
</body>
</html>