<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;

class InitialConfigsValue extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
        'config_goods_prefix' => 'BRG',
        'config_goods_count' => 0,
        'config_goods_lastcount' => '000',
        'config_consumer_prefix' => 'CNS',
        'config_consumer_count' => 0,
        'config_consumer_lastcount' => '000',
        'config_inflow_prefix' => 'INF',
        'config_inflow_count' => 0,
        'config_inflow_lastcount' => '000',
        'config_asset_prefix' => 'AST',
        'config_asset_count' => 0,
        'config_asset_lastcount' => '000',
        'config_expenditure_prefix' => 'EXP',
        'config_expenditure_count' => 0,
        'config_expenditure_lastcount' => '000',
        'config_history_prefix' => 'HIST',
        'config_history_count' => 0,
        'config_history_lastcount' => '000',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        ]);

        User::create([
            'name'     => 'Admin',
            'email'    => 'admin-sib@poltekba.ac.id',
            'password' => bcrypt('Admin-Sib123'),
        ]);
    }
}
