<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inflows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inflow_no');
            $table->string('inflow_goods_no');
            $table->string('inflow_goods_name');
            $table->string('inflow_goods_brand');
            $table->string('inflow_goods_type');
            $table->double('inflow_goods_quantity');
            $table->date('inflow_date');
            $table->timestamp('inflow_user_event_time');
            $table->boolean('void')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inflows');
    }
}
