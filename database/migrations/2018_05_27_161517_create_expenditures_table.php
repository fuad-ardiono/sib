<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenditures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('expenditure_no');
            $table->string('expenditure_consumer_no');
            $table->string('expenditure_consumer_name');
            $table->string('expenditure_goods_no');
            $table->string('expenditure_goods_name');
            $table->string('expenditure_goods_type');
            $table->string('expenditure_goods_brand');
            $table->double('expenditure_goods_quantity');
            $table->date('expenditure_date');
            $table->timestamp('expenditure_user_event_time');
            $table->boolean('void')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenditures');
    }
}
