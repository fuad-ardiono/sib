<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('config_goods_prefix');
            $table->string('config_goods_count');
            $table->string('config_goods_lastcount');
            $table->string('config_consumer_prefix');
            $table->string('config_consumer_count');
            $table->string('config_consumer_lastcount');
            $table->string('config_inflow_prefix');
            $table->string('config_inflow_count');
            $table->string('config_inflow_lastcount');
            $table->string('config_asset_prefix');
            $table->string('config_asset_count');
            $table->string('config_asset_lastcount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
