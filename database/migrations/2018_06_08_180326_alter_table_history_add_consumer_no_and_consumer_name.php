<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableHistoryAddConsumerNoAndConsumerName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history', function(Blueprint $table){
            $table->string('history_consumer_no')->nullable()->after('history_goods_quantity');
            $table->string('history_consumer_name')->nullable()->after('history_consumer_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
