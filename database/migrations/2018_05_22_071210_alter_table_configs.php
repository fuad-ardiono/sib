<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableConfigs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configs', function (Blueprint $table) {
            $table->string('config_expenditure_prefix')->after('config_asset_lastcount');
            $table->string('config_expenditure_count')->after('config_expenditure_prefix');
            $table->string('config_expenditure_lastcount')->after('config_expenditure_count');
            $table->string('config_history_prefix')->after('config_expenditure_lastcount');
            $table->string('config_history_count')->after('config_history_prefix');
            $table->string('config_history_lastcount')->after('config_history_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
